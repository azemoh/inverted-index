/* globals FileReader document */

import angular from 'angular';

import InvertedIndex from './invertedIndex';
import '../sass/style.scss';

angular.module('InvertedIndex', [])

.controller('IndexCtrl', ['$scope', ($scope) => {
  const index = new InvertedIndex();

  $scope.indexNames = [];
  $scope.currentIndex = null;
  $scope.searchResult = [];

  $scope.message = '';
  $scope.query = '';

  // Set currentIndex
  $scope.setIndex = (title) => {
    $scope.index = index.getIndex(title);
    $scope.currentIndex = title;
  };

  // Create index
  $scope.createIndex = (file) => {
    if (!file.name.toLowerCase().match(/\.json$/)) {
      $scope.setMessage('This is not a JSON file.');
      return;
    }

    const title = file.name.split('.')[0];
    const reader = new FileReader();

    // Read uploaded file
    reader.onloadend = (e) => {
      try {
        $scope.setMessage('');
        const data = JSON.parse(e.target.result);

        // Validate format
        if (!(data[0] && data[0].title)) {
          $scope.setMessage('Invalid JSON format.');
          return;
        }

        // Create index
        index.createIndex(title, data);

        // set currentIndex
        $scope.$apply(() => {
          $scope.indexNames.push(title);
          $scope.setIndex(title);
        });
      } catch (ex) {
        $scope.setMessage('Invalid JSON file.');
      }
    };

    reader.readAsBinaryString(file);
  };

  // Search
  $scope.search = () => {
    const result = index.searchIndex($scope.query, $scope.currentIndex);
    if (typeof result === 'string') {
      $scope.message = result;
    } else {
      $scope.index = result;
    }

    $scope.query = '';
  };

  // Set $scope message
  $scope.setMessage = (message) => {
    $scope.$apply(() => {
      $scope.message = message;
    });
  };
}])

/**
 * range filter turn number to array range
 * @function
 * @param {Array} input
 * @param {Number} range
 * @return {Array}
 */
.filter('range', () => (input, range) => {
  for (let i = 0; i < parseInt(range, 10); i += 1) {
    input.push(i);
  }

  return input;
});

// Document ready.
document.addEventListener('DOMContentLoaded', () => {
  // Attach file upload listener
  document.getElementById('json-file')
    .addEventListener('change', function createFile() {
      if (this.files[0]) {
        angular.element(this).scope().createIndex(this.files[0]);
      }
    });
});

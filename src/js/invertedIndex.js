/**
 * InvertedIndex class
 * @class
 */
class InvertedIndex {
  /**
   * class constructor
   * @constructor
   */
  constructor() {
    this.indexes = {};
  }

  /**
   * Get tokens in a string of text.
   * @function
   * @param {String} text text to be tokenize.
   * @return {Array} array of string tokens
   */
  static tokenize(text) {
    return text.toLowerCase()
               .replace(/[^\w\s]/g, '')
               .split(/\s+/);
  }

  /**
   * Create index
   * @function
   * @param {string} title
   * @param {Array} data
   * @return {Object} index object
   */
  createIndex(title, data) {
    const dictionary = {};

    data.forEach((docObject, index) => {
      for (const key in docObject) {
        const tokens = InvertedIndex.tokenize(docObject[key]);

        tokens.forEach((token) => {
          if (dictionary[token]) { // token is in dictionary
            if (dictionary[token].indexOf(index) === -1) {
              dictionary[token].push(index);
            }
          } else {
            dictionary[token] = [index];
          }
        });
      }
    });

    this.indexes[title] = {
      words: dictionary,
      docCount: data.length
    };

    return this.indexes[title];
  }

  /**
   * Get a particular index
   * @function
   * @param {String} title
   * @return {Object} index object
   */
  getIndex(title) {
    return this.indexes[title];
  }

  /**
   * Search Index.
   * @function
   * @param {String} query query string
   * @param {String} title title of index to be searched.
   * @returns {Object|String} search result object.
   */
  searchIndex(query, title) {
    const queryTokens = InvertedIndex.tokenize(query);
    const index = this.getIndex(title);

    if (!index) {
      return `Index with ${title} does not exist.`;
    }

    const result = {
      words: {},
      docCount: index.docCount
    };

    queryTokens.forEach((token) => {
      if (index.words[token]) {
        result.words[token] = index.words[token];
      }
    });

    return Object.keys(result.words).length > 0 ?
      result : 'no word found';
  }

}

module.exports = InvertedIndex;

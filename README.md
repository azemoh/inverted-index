# Inverted Index

[![Build Status][travis-image]][travis-url]
[![Code Climate][codeclimate-image]][codeclimate-url]
[![Test Coverage][test-coverage-image]][test-coverage-url]
[![Issue Count][issues-image]][issues-url]

## Introduction

Inverted index takes a JSON array of text objects and creates an index from the array. The index allows a user to search for text blocks in the array that contain a specified collection of words.

## Features
- Create indexes from uploaded file.
- Find a particular index.
- Full text search of created indexes.

## Technologies
- Node.js
- EchmaScript 6 (JavaScript 2015)
- Angular.js

## Useage
The app can be deployed directly to Heroku by clicking this button

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/azemoh/inverted-index)

Or you can clone this repository and run the app locally.

## Local Development
- Install npm dependencies `npm install`
- Start a local server `npm run dev`
- To test the app run: `npm test`

## Contributing
1. Fork this repositry to your account.
1. Clone your repositry: `git clone git@github.com:your-username/inverted-index.git`
1. Create your feature branch: `git checkout -b new-feature`
1. Commit your changes: `git commit -m "did something"`
1. Push to the remote branch: `git push origin new-feature`
1. Open a pull request.

[travis-url]: https://travis-ci.org/azemoh/inverted-index
[travis-image]: https://travis-ci.org/azemoh/inverted-index.svg

[codeclimate-url]: https://codeclimate.com/github/azemoh/inverted-index
[codeclimate-image]: https://codeclimate.com/github/azemoh/inverted-index/badges/gpa.svg

[test-coverage-url]: https://codeclimate.com/github/azemoh/inverted-index/coverage
[test-coverage-image]: https://codeclimate.com/github/azemoh/inverted-index/badges/coverage.svg

[issues-image]: https://codeclimate.com/github/azemoh/inverted-index/badges/issue_count.svg
[issues-url]: https://codeclimate.com/github/azemoh/inverted-index

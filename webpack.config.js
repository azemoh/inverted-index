const debug = process.env.NODE_ENV !== 'production';

const Path = require('path');
const Webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTextPlugin('css/style.css');

const webpackConfig = {
  context: Path.resolve(__dirname, './src'),
  devtool: debug ? 'inline-sourcemap' : null,
  entry: {
    app: './js/app.js'
  },
  output: {
    path: Path.resolve(__dirname, './public'),
    filename: 'js/[name].min.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      }, {
        test: /\.(css|scss)$/,
        loader: extractCSS.extract(['css', 'sass'])
      }
    ]
  },
  plugins: debug ? [] : [
    new Webpack.optimize.OccurenceOrderPlugin(),
    new Webpack.optimize.DedupePlugin(),
    new Webpack.optimize.UglifyJsPlugin({
      mangle: false,
      sourcemap: false,
      compressor: { warnings: false }
    })
  ]
};

webpackConfig.plugins.push( extractCSS );

module.exports = webpackConfig;

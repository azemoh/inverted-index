const expect = require('chai').expect;

const books = require('./books.json');
const InvertedIndex = require('../src/js/invertedIndex');


describe('InvertedIndex Class', () => {
  beforeEach(() => {
    this.invertedIndex = new InvertedIndex();
    this.index = this.invertedIndex.createIndex('books', books);
  });

  describe('Constructor', () => {
    it('can create inverted index instance', () => {
      expect(this.invertedIndex).to.be.an('object');
      expect(this.invertedIndex).to.be.instanceof(InvertedIndex);
    });

    it('has default empty indexes object', () => {
      expect(typeof this.invertedIndex.indexes).to.equal('object');
    });
  });

  describe('Tokenize', () => {
    it('should make an array of tokens', () => {
      expect(InvertedIndex.tokenize(books[0].title))
        .to.deep.equal(['alice', 'in', 'wonderland']);
    });

    it('filters out symbols', () => {
      expect(InvertedIndex.tokenize('alice # in* Won@derland'))
        .to.deep.equal(['alice', 'in', 'wonderland']);
    });
  });

  describe('CreateIndex', () => {
    it('creates index', () => {
      expect(this.index.words.a).to.deep.equal([0, 1]);
      expect(this.index.words.alice).to.deep.equal([0]);
    });

    it('saves document length', () => {
      expect(this.index.docCount).to.equal(2);
    });
  });

  describe('GetIndex', () => {
    it('gets a particular index', () => {
      const index = this.invertedIndex.getIndex('books');

      expect(index.words.a).to.deep.equal([0, 1]);
      expect(index.words.alice).to.deep.equal([0]);
    });
  });

  describe('SearchIndex', () => {
    it('should return "not exist" if index does not exist', () => {
      expect(this.invertedIndex.searchIndex('alice in wonderland', 'movies'))
        .to.equal('Index with movies does not exist.');
    });

    it('should return "not found" for words not in index', () => {
      expect(this.invertedIndex.searchIndex('', 'books'))
        .to.equal('no word found');
    });

    it('returns object with search words', () => {
      const result = this.invertedIndex
                         .searchIndex('alice in wonderland', 'books');
      expect(result).to.have.deep.property('words.alice');
      expect(result).to.have.deep.property('words.wonderland');
      expect(result).to.have.deep.property('words.in');
    });
  });
});
